#!/bin/bash

PROJECT_ROOT=`git rev-parse --show-toplevel`
ANSIBLE_ROOT=$PROJECT_ROOT/tools/ansible

cd $ANSIBLE_ROOT

ansible-playbook rax_spawn.yml -i inventory/rackspace/
#!/bin/bash

#
#
# This is helper script to rync the current ansible workarea up to a remote host. It expects

PROJECT_ROOT=`git rev-parse --show-toplevel`
ANSIBLE_ROOT=$PROJECT_ROOT/tools/ansible/

DEST_HOST=$1

if [ X$DEST_HOST == X ]
then
	echo no target server provided!
	exit 1
fi
rsync -ave ssh $ANSIBLE_ROOT ansible@$1:/home/ansible/weightwatchers/
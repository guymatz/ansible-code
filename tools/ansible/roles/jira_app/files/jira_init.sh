#!/bin/bash
#
# chkconfig: 345 99 28
# description: Starts/Stops Apache Jira
#
 
#Location of JAVA_HOME (bin files)
export JAVA_HOME=/usr/lib/jvm/jre
 
#Add Java binary files to PATH
export PATH=$JAVA_HOME/bin:$PATH
 
#CATALINA_HOME is the location of the bin files of Jira  
export CATALINA_HOME=/opt/atlassian/jira
 
#CATALINA_BASE is the location of the configuration files of this instance of Jira
export CATALINA_BASE=$CATALINA_HOME

# PID_FILE To check for 
export PID_FILE=$CATALINA_HOME/work/catalina.pid
 
#TOMCAT_USER is the default user of jira
export TOMCAT_USER=jira
 
#TOMCAT_USAGE is the message if this script is called without any options
TOMCAT_USAGE="Usage: $0 {\e[00;32mstart\e[00m|\e[00;31mstop\e[00m|\e[00;32mstatus\e[00m|\e[00;31mrestart\e[00m}"
 
#SHUTDOWN_WAIT is wait time in seconds for java proccess to stop
SHUTDOWN_WAIT=20
 
jira_pid() {
  pid_from_ps=`ps -fe | grep $CATALINA_BASE | grep -v grep | tr -s " "|cut -d" " -f2`
  pid_from_file=`cat $PID_FILE 2> /dev/null`

#  if [[ -f $PID_FILE && $pid_from_ps != $pid_from_file ]]; then
#    echo -e "\e[00;31mJira is running but pid file is inconsistemt (pid: $pid)\e[00m"
#    exit 1
#  else
#    echo $pid_from_ps
#  fi
  echo $pid_from_ps
}
 
start() {
  pid=$(jira_pid)
  if [ -n "$pid" ]; then
    echo -e "\e[00;31mJira is already running (pid: $pid)\e[00m"
  else
    # Start jira
    echo -e "\e[00;32mStarting jira\e[00m"
    #ulimit -n 100000
    #umask 007
    #/bin/su -p -s /bin/sh jira
    if [ `user_exists $TOMCAT_USER` = "1" ]; then
      su $TOMCAT_USER -c $CATALINA_HOME/bin/startup.sh
    else
      sh $CATALINA_HOME/bin/startup.sh
    fi
    status
  fi
  return 0
}
 
status(){
  pid=$(jira_pid)
  if [ -n "$pid" ]; then echo -e "\e[00;32mJira is running with pid: $pid\e[00m" && exit 0
  else echo -e "\e[00;31mJira is not running\e[00m" && exit 1
  fi
}
 
stop() {
  pid=$(jira_pid)
  if [ -n "$pid" ]
  then
    echo -e "\e[00;31mStoping Jira\e[00m"
    #/bin/su -p -s /bin/sh jira
    sh $CATALINA_HOME/bin/shutdown.sh
 
    let kwait=$SHUTDOWN_WAIT
    count=0;
    until [ `ps -p $pid | grep -c $pid` = '0' ] || [ $count -gt $kwait ]
    do
      echo -n -e "\n\e[00;31mwaiting for processes to exit\e[00m";
      sleep 1
      let count=$count+1;
    done
 
    if [ $count -gt $kwait ]; then
      echo -n -e "\n\e[00;31mkilling processes which didn't stop after $SHUTDOWN_WAIT seconds\e[00m"
      kill -9 $pid
    fi
  else
    echo -e "\e[00;31mJira is not running\e[00m"
  fi
 
  return 0
}
 
user_exists(){
  if id -u $1 >/dev/null 2>&1; then
    echo "1"
  else
    echo "0"
  fi
}
 
case $1 in
 
  start)
  start
  ;;
       
  stop)  
  stop
  ;;
       
  restart)
  stop
  start
  ;;
       
  status)
  status
       
  ;;
       
  *)
  echo -e $TOMCAT_USAGE
  ;;
esac    
exit 0

import sys

# ADDITIONAL_APPS = (
#    'debug_toolbar',
# )
# ADDITIONAL_MIDDLEWARE = (
#    'debug_toolbar.middleware.DebugToolbarMiddleware',
# )
# INTERNAL_IPS = ('71.202.85.222',)


# def custom_show_toolbar(request):
#    return True

# DEBUG_TOOLBAR_CONFIG = {
#    'INTERCEPT_REDIRECTS': False,
#    'SHOW_TOOLBAR_CALLBACK': custom_show_toolbar,
# }

GOOGLE_OAUTH2_CLIENT_ID = \
    "1020369041627-1gokafhb0de35f3mgo7vkt130kcptcg5.apps.googleusercontent.com"
GOOGLE_OAUTH2_CLIENT_SECRET = '3AkUyyepJlWseDE61AGS8ldi'

USE_PROD_DB = True

DEBUG = False

LOG_ROOT = '/var/log/wello/'

TESTING = sys.argv[1:2] == ['test']
if TESTING:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'wello_prototype', # Or path to database file if using sqlite3.
        }
    }
elif USE_PROD_DB:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': 'wello_prod_test',  # Or path to database file if using sqlite3.
            'USER': 'django',  # Not used with sqlite3.
            'PASSWORD': 'djangoSQLw3110',  # Not used with sqlite3.
            'HOST': '',  # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',  # Set to empty string for default. Not used with sqlite3.
            'OPTIONS': {
                "init_command": "SET storage_engine=INNODB",
            }
        }
    }

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '127.0.0.1:6379',
        'OPTIONS': {
            'DB': 1,
            'PARSER_CLASS': 'redis.connection.HiredisParser'
        },
    },
}
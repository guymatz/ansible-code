# ------------------------------------------------------------------------
# Configuration file for running Apache Active MQ as standalone provider
#
# This file overwrites the predefined settings of the sysv init-script
#

ACTIVEMQ_HOSTNAME="$(hostname)"

ACTIVEMQ_BASE={{activemq_base}}
ACTIVEMQ_DATA={{activemq_data}}
ACTIVEMQ_HOME={{activemq_home}}
ACTIVEMQ_CONF="$ACTIVEMQ_HOME/conf"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dsysname=${ACTIVEMQ_HOSTNAME}"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dactivemq.jmx.user={{activemq_console_jmx_user}} -Dactivemq.jmx.password={{activemq_console_jmx_password}}" 

ACTIVEMQ_PIDFILE="$ACTIVEMQ_DATA/activemq-${ACTIVEMQ_HOSTNAME}.pid"

# ACTIVEMQ_SUNJMX_START="$ACTIVEMQ_SUNJMX_START -Dcom.sun.management.jmxremote"
ACTIVEMQ_SUNJMX_START="$ACTIVEMQ_SUNJMX_START -Dcom.sun.management.jmxremote.port=1099 "
ACTIVEMQ_SUNJMX_START="$ACTIVEMQ_SUNJMX_START -Dcom.sun.management.jmxremote.password.file=${ACTIVEMQ_CONF}/jmx.password"
ACTIVEMQ_SUNJMX_START="$ACTIVEMQ_SUNJMX_START -Dcom.sun.management.jmxremote.access.file=${ACTIVEMQ_CONF}/jmx.access"
ACTIVEMQ_SUNJMX_START="$ACTIVEMQ_SUNJMX_START -Dcom.sun.management.jmxremote.ssl=false"

ACTIVEMQ_SUNJMX_CONTROL=""

#ACTIVEMQ_USER="activemq"

ACTIVEMQ_TMP="$ACTIVEMQ_HOME/tmp"

## Create the data directory if it does not yet exist
if [ ! -d "$ACTIVEMQ_DATA" ]
then
    su -c "mkdir $ACTIVEMQ_DATA" - $ACTIVEMQ_USER;
fi

# Uncomment to enable audit logging
#ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dorg.apache.activemq.audit=true"

##
## Determine the size of the server using /proc.
##
getSystemMemorySize () {
	typeset size

	size=$(awk '/MemTotal:/ { printf("%d\n", ($2 / 1024 )); }' /proc/meminfo)
	if [ -z "$size" ]
	then
		size="8192"
	fi

	echo "$size"
}

##
## Set the JVM to half the size of the system memory
##
#SYS_SIZE="$(getSystemMemorySize)"
#JVM_SIZE="$(( SYS_SIZE / 2 ))"
#ACTIVEMQ_OPTS_MEMORY="-Xms${JVM_SIZE}M -Xmx${JVM_SIZE}M"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS {{activemq_jvm_opts}}"

##
## Set the per-destination memory limit; note this passes into the JVM as a property and can be used in the
##  activemq.xml file as such.
##
#DEST_LIMIT="$(( JVM_SIZE / 10 ))"
#DEST_LIMIT_MB="${DEST_LIMIT}mb"
#BROKER_STORE_LIMIT="100gb"
#BROKER_TEMP_LIMIT="50gb"

DEST_LIMIT_MB="{{activemq_dest_limit}}"
BROKER_STORE_LIMIT="{{activemq_store_limit}}"
BROKER_TEMP_LIMIT="{{activemq_tmp_limit}}"

ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Ddestination.limit.mem=${DEST_LIMIT_MB}"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dbroker.limit.store=${BROKER_STORE_LIMIT}"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dbroker.limit.temp=${BROKER_TEMP_LIMIT}"
ACTIVEMQ_OPTS="$ACTIVEMQ_OPTS -Dbroker.name=${ACTIVEMQ_HOSTNAME}"

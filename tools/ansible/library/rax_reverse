#!/usr/bin/python


# MBS - adapted from https://gist.github.com/lvonk/7550884


# Place this file in for instance my_project/modules and run playbook with
#   ansible-playbook project.yml -i development --module-path modules
# Usage:
# - hosts: local-app
#     connection: local
#     user: root
#     tasks:
#       - name: Rackspace | Add PTR records
#         local_action:
#         module: rax_reverse
#         credentials: ~/.my_rax_credentials_file
#         hostname: "foo.example.com"
# - hosts: autoscalinggroup-app
#     connection: local
#     user: root
#     tasks:
#       - name: Rackspace | Add PTR records
#         local_action:
#         module: rax_reverse
#         credentials: ~/.my_rax_credentials_file
#         hostname: {{ inventory_hostname }}
#         domain: "bar.com"
# As a result it will create 2 reverse DNS records, for both ipv4 and ipv6.
# As a result it will create 2 reverse DNS records, for both ipv4 and ipv6.
# TODO: Make documentation according to ansible manual...
# - run from a playbook
 
import pyrax
 
def main():
  module = AnsibleModule(
    argument_spec = dict(
      credentials = dict(required=True),
      hostname    = dict(required=True),
      # domain needs to be added for auto-scaling hosts which don't get the
      # domain added to their hostname by default
      domain      = dict(required=False)
      )
  )
 
  pyrax.set_setting("identity_type", "rackspace")
  credentials = os.path.expanduser(module.params['credentials'])
  hostname = module.params['hostname']
  domain = module.params['domain']
  fqdn = hostname
  if domain:
    fqdn = hostname + '.' + domain
  pyrax.set_credential_file(credentials)
 
  cloudservers = pyrax.cloudservers
  server = next((x for x in cloudservers.servers.list() if x.name in [hostname, fqdn] ), None)
  if server:
    ipv4 = getattr(server, 'accessIPv4')
    ipv6 = getattr(server, 'accessIPv6')
    
    dns = pyrax.cloud_dns
    ptr_records = dns.list_ptr_records(server)
    changed_ptr = False
    records = [] 
    for ip in [ipv4, ipv6]:
      ptr_record = next((x for x in ptr_records if (x.data == ip and x.name in [hostname, fqdn] )), None)
      if not ptr_record:
        changed_ptr = True
        new_record = {
          "name": fqdn,
          "type": "PTR",
          "data": ip,
          "ttl": 7200
        }
        records.append(new_record) 
 
    if records:
      message = dns.add_ptr_records(server, records)
    else:
      message = "All PTR records already exist..."
 
    module.exit_json(changed=changed_ptr, msg=message)
  else:
    module.fail_json(msg="Host {0} not found.".format(hostname))
 
# include magic from lib/ansible/module_common.py
#<<INCLUDE_ANSIBLE_MODULE_COMMON>>
main()

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end



if ENV["AUTOBAHN_CACHE_CENTOS"]
  puts "Overriding CENTOS cache passthrough to #{ENV['AUTOBAHN_CACHE_CENTOS']}"
  CENTOS_CACHE = ENV["AUTOBAHN_CACHE_CENTOS"]
else
  # if OS.windows?
  #   CENTOS_CACHE="#{ENV['HOMEDRIVE']}#{ENV['HOMEPATH']}/cache/centos"
  # else
  #   CENTOS_CACHE="#{ENV['HOME']}/cache/centos"
  # end
  CENTOS_CACHE="#{PROJECT_ROOT}/../cache/centos"
end

FileUtils.mkdir_p CENTOS_CACHE

if ENV["AUTOBAHN_CACHE_RHEL"]
  puts "Overriding RHEL cache passthrough to #{ENV['AUTOBAHN_CACHE_RHEL']}"
  RHEL_CACHE = ENV["AUTOBAHN_CACHE_RHEL"]
else
  # if OS.windows?
  #   RHEL_CACHE="#{ENV['HOMEDRIVE']}#{ENV['HOMEPATH']}/cache/rhel"
  # else
  #   RHEL_CACHE="#{ENV['HOME']}/cache/rhel"
  # end
  RHEL_CACHE="#{PROJECT_ROOT}/../cache/rhel"
end

FileUtils.mkdir_p RHEL_CACHE


if ENV["AUTOBAHN_CACHE_GENERIC"]
    puts "Overriding GENERIC cache passthrough to #{ENV['AUTOBAHN_CACHE_GENERIC']}"
    GENERIC_CACHE = ENV["AUTOBAHN_CACHE_GENERIC"]
else
    # if OS.windows?
    #   GENERIC_CACHE="#{ENV['HOMEDRIVE']}#{ENV['HOMEPATH']}/cache/generic"
    # else
    #   GENERIC_CACHE="#{ENV['HOME']}/cache/generic"
    # end
    GENERIC_CACHE="#{PROJECT_ROOT}/../cache/generic"
end

puts "GENERIC cache is now #{GENERIC_CACHE}"
FileUtils.mkdir_p GENERIC_CACHE



# config.vm.synced_folder ENV["AUTOBAHN_CACHE_CENTOS"], "/var/cache/yum"
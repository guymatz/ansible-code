Getting Ready:

* Install Virtualbox from MSI on this drive
* Install Vagrant from MSI on this drive

	At this point vagrant should be in your path and executable from the command prompt.

	Test that by typing 'vagrant --version'

	If that doesnt work, you may need to update your path settings.


Next, install one vagrant plugin:

* vagrant plugin install vagrant-hostsupdater 


You are now ready to use the image.



Using the image:

Useful commands - all must be run from the directory containing the vagrant file

To bring up the box: 'vagrant up'
To shut down the box: 'vagrant destroy'
To login to the box: 'vagrant ssh' - after which you may 'sudo su -l' if you like to get root. 

Mule MMC Console: http://esb.user.vagrant.dev:8585/mmc-3.5.0/     username/password is admin/admin
Active MQ Management Console: http://esb.user.vagrant.dev:8161/   username/password is admin/admin

Mule and MQ installs are under /opt; the mule hot-deploy directory is in /opt/mule/mule-enterprise-3.5.0/apps.

When binding mule endpoints, its best to bind to either all addresses, or 192.168.33.233, which is the external address associated with esb.user.vagrant.dev.

The host system folder containing the vagrant file is visible read/write on the guest VM under the path /vagrant. This is a useful way to move files in and out of the system. One example use is for repeated hot-deploys. 

I have included a script baked into the image to help with hot-deploys.

To use it, under the mule_deploy directory on the host system, create subdirectories for each mule app. They should have the following structure:

Example, for app 'FOO'

mule_deploy/FOO/mule-config.xml
mule_deploy/FOO/lib/    put custom jars here, if any.

To initiate a deploy, either run 'mule_local_deploy' from within the vm, or run deploy.bat from the host system.


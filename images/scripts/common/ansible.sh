#!/bin/bash -eux

#register with RHN server
#wget -qO - http://nyaprhproxy01.na.weightwatchers.net/pub/bootstrap/bootstrap-rh6.sh | /bin/bash


# now baked in to packer ks.cfg
# yum -y groupinstall "Development tools"
# yum -y install python-devel python-setuptools

easy_install pip
pip install paramiko PyYAML jinja2 httplib2 
pip install --upgrade setuptools
pip install --upgrade setuptools
pip install -I ansible==1.6.6


# OLD!
# Install EPEL repository.
# rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

# Install Ansible.

# yum -y install python-paramiko PyYAML python-jinja2-26 python-httplib2

# yum -y install ansible --sl
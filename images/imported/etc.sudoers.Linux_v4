# Version: 1.6 2010/03/10

## Sudoers allows particular users to run various commands as
## the root user, without needing the root password.
##
## Examples are provided at the bottom of the file for collections
## of related commands, which can then be delegated out to particular
## users or groups.
## 
## This file must be edited with the 'visudo' command.

## Host Aliases
## Groups of machines. You may prefer to use hostnames (perhap using 
## wildcards for entire domains) or IP addresses instead.
# Host_Alias     FILESERVERS = fs1, fs2
# Host_Alias     MAILSERVERS = smtp, smtp2

## User Aliases
## These aren't often necessary, as you can use regular groups
## (ie, from files, LDAP, NIS, etc) in this file - just use %groupname 
## rather than USERALIAS
# User_Alias ADMINS = jsmith, mikem


## Command Aliases
## These are groups of related commands...

## Networking
# Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /bin/ping, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool, /sbin/ethtool
Cmnd_Alias NETWORKING = /sbin/route, /sbin/ifconfig, /sbin/dhclient, /usr/bin/net, /sbin/iptables, /usr/bin/rfcomm, /usr/bin/wvdial, /sbin/iwconfig, /sbin/mii-tool, /sbin/ethtool

## Installation and management of software
Cmnd_Alias SOFTWARE = /bin/rpm, /usr/bin/up2date, /usr/bin/yum

## Services
Cmnd_Alias SERVICES = /sbin/service, /sbin/chkconfig

## Updating the locate database
Cmnd_Alias LOCATE = /usr/bin/updatedb

## Storage
Cmnd_Alias STORAGE = /sbin/fdisk, /sbin/sfdisk, /sbin/parted, /sbin/partprobe, /bin/mount, /bin/umount

## Delegating permissions
# Cmnd_Alias DELEGATING = /usr/sbin/visudo, /bin/chown, /bin/chmod, /bin/chgrp 
Cmnd_Alias DELEGATING = /bin/chown, /bin/chmod, /bin/chgrp 

## Processes
Cmnd_Alias PROCESSES = /bin/nice, /bin/kill, /usr/bin/kill, /usr/bin/killall

## Drivers
Cmnd_Alias DRIVERS = /sbin/modprobe

Cmnd_Alias     HALT = /sbin/halt, /sbin/shutdown, /sbin/reboot
Cmnd_Alias     KILL = /bin/kill
Cmnd_Alias     VI = /bin/vi
Cmnd_Alias     SHELLS = /bin/sh, /bin/csh, /bin/ksh, /bin/tcsh, /usr/kerberos/bin/rsh, /bin/zsh
Cmnd_Alias     SU = /bin/su
Cmnd_Alias     VISUDO = /usr/sbin/visudo
Cmnd_Alias     PAGERS = /bin/more, /usr/bin/more, /usr/bin/pg, /usr/bin/less
Cmnd_Alias     USERS = /usr/sbin/useradd, /usr/sbin/userdel
Cmnd_Alias     FILES = /etc/group, /etc/passwd, /etc/shadow


# Defaults specification

#
# Disable "ssh hostname sudo <cmd>", because it will show the password in clear. 
#         You have to run "ssh -t hostname sudo <cmd>".
#
Defaults    logfile=/var/log/sudolog
Defaults    passwd_tries=5
Defaults    log_year

Defaults    requiretty

Defaults    env_reset
Defaults    env_keep = "COLORS DISPLAY HOSTNAME HISTSIZE INPUTRC KDEDIR \
                        LS_COLORS MAIL PS1 PS2 QTDIR USERNAME \
                        LANG LC_ADDRESS LC_CTYPE LC_COLLATE LC_IDENTIFICATION \
                        LC_MEASUREMENT LC_MESSAGES LC_MONETARY LC_NAME LC_NUMERIC \
                        LC_PAPER LC_TELEPHONE LC_TIME LC_ALL LANGUAGE LINGUAS \
                        _XKB_CHARSET XAUTHORITY"

## Next comes the main part: which users can run what software on 
## which machines (the sudoers file can be shared between multiple
## systems).
## Syntax:
##
## 	user	MACHINE=COMMANDS
##
## The COMMANDS section may have other options added to it.
##
## Allow root to run any commands anywhere 
root	ALL=(ALL) 	ALL

# Who can and can't do what?
# dbawheel can not change password thru sudo
%dba    ALL = !/usr/bin/passwd
# no one can do anything to the sudolog file.
%dba    ALL = !/var/log/sudolog

# dbawheel can not halt the system or change shells, but can kill their own processes.
%dba    ALL = !HALT, !SHELLS, KILL
%dba    ALL = !USERS

%dba    ALL = !NETWORKING
%dba    ALL = !SOFTWARE
%dba    ALL = !SERVICES
%dba    ALL = !LOCATE
%dba    ALL = !DELEGATING
%dba    ALL = !PROCESSES
%dba    ALL = !DRIVERS



# This will allow the dbawheel group to exec more and vi but will
# prevent shell escapes, just incase the default misses it.
#%dba    ALL = NOEXEC: PAGERS, VI

# Allow anyone in the dbawheel group to sudo to oracle, but not to root
# without a passwd and this should also prevent 'sudo oracle -c "command"'
%dba    ALL = NOPASSWD: /bin/su - oracle
%dba    ALL = NOPASSWD: /bin/su - orawwiprd
%dba  ALL = NOPASSWD: !/bin/su - root

## Allows members of the 'sys' group to run networking, software, 
## service management apps and more.
# %sys ALL = NETWORKING, SOFTWARE, SERVICES, STORAGE, DELEGATING, PROCESSES, LOCATE, DRIVERS

## Allows people in group wheel to run all commands
%wheel	ALL=(ALL)	ALL

## Same thing without a password
# %wheel	ALL=(ALL)	NOPASSWD: ALL

## Allows members of the users group to mount and unmount the 
## cdrom as root
# %users  ALL=/sbin/mount /mnt/cdrom, /sbin/umount /mnt/cdrom

## Allows members of the users group to shutdown this system
# %users  localhost=/sbin/shutdown -h now


#!/bin/sh
##/bin/bash -x /mnt/itadmin/bin/regserver.sh
yum -y install nfs-utils* compat-db* compat-libstdc++* glibc* glibc-2* libaio* libXpm* libXpm* compat-libstdc++* libgcc-4* ksh* make* gcc-4* compat-libcap1* gcc-c++* libstdc++-devel* sysstat* glibc-devel.i686* sharutils* tigervnc-server tigervnc-server-module libXfont pixman xterm xorg-x11-twm samba-winbind* device-mapper-multipath*

/bin/bash -x /mnt/itadmin/bin/ad_auth_linux/install.sh

yum -y update
##
cp /mnt/itadmin/KickStart/files/multipath.conf.default /etc/multipath.conf
## Fix /etc/profile color
cat /mnt/itadmin/bin/etcprofilefix >>/etc/profile
##
##
### Fix HISTSIZE=1000 inside of /etc/profile to HISTSIZE=10000
cp /etc/profile /etc/profile.`date +%Y%m%d`
sed -i "s/HISTSIZE=1000/HISTSIZE=10000/g" /etc/profile 

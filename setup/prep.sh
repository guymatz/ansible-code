#!/bin/bash
vagrant plugin list | grep -q vagrant-hostsupdater || vagrant plugin install vagrant-hostsupdater

#ln -s ~/Dropbox/cache/generic-cache environments/cache/generic
#ln -s ~/Dropbox/cache/centos-cache environments/cache/centos

AUTOBAHN_CACHE_GENERIC=~/Dropbox/cache/generic-cache
AUTOBAHN_CACHE_CENTOS=~/Dropbox/cache/centos-cache

export AUTOBAHN_CACHE_GENERIC
export AUTOBAHN_CACHE_CENTOS